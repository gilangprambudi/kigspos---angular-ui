import {Category} from './category';
import {SubCategory} from './subcategory';


export class Product{
    id:number;
    sku:string;
    name:string;
    category:Category;
    subCategory:Category;
    weight:any;
    karat:number;
    price:number;
    date_in:Date;
}