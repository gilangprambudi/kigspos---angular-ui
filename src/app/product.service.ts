import { Injectable } from '@angular/core';
import { Product } from './../model/product';
import { from, Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})


export class ProductService {

  constructor(private http: HttpClient) { }

  test():Observable<Product[]>{
    let apiURL = environment.url + '/pos/product';
    return this.http.get<Product[]>(apiURL);
  }

  addProduct(search_param:String){
    let apiURL = environment.url + '/pos/product/search?sku='+search_param;
    console.log(apiURL);
    return this.http.get(apiURL).toPromise();
  }

  createTransaction(data:any){
    let apiURL = environment.url + '/pos/create-transaction';
    return this.http.post(apiURL, data).toPromise();
  }

  createSales(data:any){
    let apiURL = environment.url + '/pos/create-sales';
    return this.http.post(apiURL, data).toPromise();
  }

  getProducts(): Product[] {
    let product: Product[];

    // Create an Observable out of a promise
    const data = from(fetch(environment.url + '/pos/product'));
    // Subscribe to begin listening for async result
    data.subscribe({
      next(response) { console.log(response.text()); },
      error(err) { console.error('Error: ' + err); },
      complete() { console.log('Completed');}
    });


    return product;

  }

  testMethod(){
    this.http.post('http://127.0.0.1/pos/test', {'param' : 'test-param'}, {responseType: 'text'})
    .subscribe(
      data => {
       //apparently following line is not needed.
       //this.router.navigateByUrl(this.url);
       window.location.href = 'http://127.0.0.1/pos/test';
      },
      error => {
        console.log("Error");
        console.log(error);
        
          },
      () => {
        console.log("POST is completed");
      });
  }
}
