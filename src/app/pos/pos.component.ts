import { Component, OnInit, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormControl, FormBuilder } from '@angular/forms';
import { Product } from '../../model/product';
import { Category } from '../../model/category';
import { SubCategory } from '../../model/subcategory';
import { ProductService } from '../product.service';
import * as anything from 'jquery';
import { RouterModule, Routes } from '@angular/router';
import { environment } from '../../environments/environment.prod';

@Component({
  selector: 'app-pos',
  providers: [DatePipe],
  templateUrl: './pos.component.html',
  styleUrls: ['./pos.component.css',
    '../../../node_modules/bootstrap/dist/css/bootstrap.min.css',
    '../../../node_modules/font-awesome/css/font-awesome.min.css']
})

export class PosComponent implements OnInit {


  barcode = new FormControl('');
  name = new FormControl('');
  test = new FormControl('');
  payAmount = new FormControl('');
  transactionId = new FormControl('');

  prevSearchedBarcode: string;
  selectedProduct: Product;
  checkoutForm;
  products: Product[] = [];
  totalPrice: number = 0;
  totalQty: number = 0;
  selectedIndex: number;
  currentDate;
  creatingTransactionLoad:boolean = false;


  constructor(private datePipe: DatePipe, private productService: ProductService, private formBuilder: FormBuilder, private _elementRef: ElementRef) {
    this.currentDate = this.datePipe.transform(this.getDate(), 'yyyy-MM-dd hh:mm:ss');

    setInterval(() => {

      this.currentDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd hh:mm:ss')
    }, 1000)

    this.checkoutForm = this.formBuilder.group({
      name: '',
      address: ''
    });
  }


  onSubmit() {
    // $('.formTest').submit();
    this.test.setValue("Test");
    this._elementRef.nativeElement.querySelector('.formTest').submit();
  }

  getDate(): Date {
    return new Date();
  }


  addProduct(): void {

    this.productService.addProduct(this.barcode.value).then((response) => {

      if (response != null) {
        let productTemp: Product = {
          id: response['id'],
          sku: response['sku'],
          name: response['name'],
          weight: response['weight'],
          karat: response['karat'],
          price: response['price'],
          category: {
            id: response['category_id'],
            name: response['goods_category']['name'],
          },

          subCategory: {
            id: response['sub_category_id'],
            name: response['goods_sub_category']['name']
          },
          date_in: new Date()

        }
        this.products.push(productTemp);
        this.totalPrice += productTemp.price;
        this.totalQty++;
        console.table(this.products);


      } else {
        $(document).ready(function () {
          (<any>$('#productNotFoundModal')).modal('show');
        });
      }



    }
    );

  }

  createTransaction() {
    this.creatingTransactionLoad = true;
    for (var i = 0; i < this.products.length; i++) {
      if (this.products[i].id == null) return false;
    }
    var self = this;
    // 'user_id' is still hardcoded
    this.productService.createTransaction({ 'price_total': this.totalPrice, 'user_id': 1, 'customer_name': 'dummy_customer2' }).then(
      (response) => {

        console.log(response);

        let insertSales = async function () {
          for (var i = 0; i < self.products.length; i++) {

            await self.productService.createSales({ 'transaction_id': response['id'], 'goods_id': self.products[i].id });
            console.log( self.products[i].id);
          }
          
        }

        insertSales().then(() => {
          self.emptyTable();
          self.transactionId.setValue(response['id']);
          $('.formCheckout').submit();
        });


      }
    )


  }




  submitBarcode(): void {
    this.addProduct();
    this.prevSearchedBarcode = this.barcode.value;
    this.barcode.setValue("");
  }


  emptyTable(): void {
    this.products = [];
    this.totalPrice = 0;
    this.selectedProduct = null;
    this.selectedIndex = null;
    this.totalQty = 0;
  }


  selectItem(index: number): void {
    this.selectedProduct = this.products[index];
    this.selectedIndex = index;
  }

  removeItem(): void {
    this.selectedProduct = null;
    this.products.splice(this.selectedIndex, 1);
    this.selectedIndex = null;
    this.updatePosProperty();
  }

  updatePosProperty(): void {
    this.totalQty = 0;
    this.totalPrice = 0;
    for (let i = 0; i < this.products.length; i++) {
      this.totalPrice += this.products[i].price;
    }
    this.totalQty = this.products.length;

  }

  ngOnInit() {


  }

}
